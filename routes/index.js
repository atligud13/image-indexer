var express = require('express');
var router = express.Router();
var walk = require('../utils/walk');

/* GET home page. */
router.get('/', function(req, res, next) {
	var db = req.db;
	var collection = db.get('filecollection');
	collection.count({}, function(error, count) {
		if(count > 0) {
			res.redirect('files/1');
		} else {
			res.render('index', { });
		}
	});
});

/* Render data list */
router.get('/files/:page', function(req, res) {
	var db = req.db;
	var collection = db.get('filecollection');
	var limit = 50;
	if(req.query.limit) limit = req.query.limit;
	/* Setting page values */
	var page = Number(req.params.page);
	var previousPage = page - 1;
	var nextPage = page + 1;
	if(previousPage == 0) previousPage = 1;
	var skip = (page - 1) * limit;
	collection.count({}, function(error, count) {
		collection.find({},
						{ limit: limit, skip: skip },
						function(e, docs) {
							res.render('files', {
								'list': docs,
								'nextPage': nextPage,
								'previousPage': previousPage,
								'currPage': page,
								'count': count,
								'limit': limit
							});
		});
	});
});

/* Return data in JSON format */
router.get('/filesjson', function(req, res) {
	var db = req.db, limit = 10, page = 1;
	/* Checking for query params */
	if(req.query.take) limit = req.query.take;
	if(req.query.page && req.query.page >= 1) page = req.query.page;
	var skip = (page - 1) * limit;
	var collection = db.get('filecollection');
	collection.count({}, function(error, count) {
		collection.find({},
						{ limit: limit, skip: skip },
						function(e, docs) {
						res.send({
							count: count,
							files: docs
						});
		});
	});
});

/* Fetch images */
router.post('/getimages', function(req, res) {
	var db = req.db;
	var collection = db.get('filecollection');
	/* Start by dropping the collection */
	collection.drop(function() {
		/* Walk through all images and save them to the db
		   Once finished, we redirect the user to the file list */
		walk(process.env.HOME, collection, function(err, results) {
			if (err) throw err;
			collection.find({},{},function(e, docs) {
				res.redirect('files/1');
			});
		});
	});
});

module.exports = router;
