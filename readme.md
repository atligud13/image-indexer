# Image Indexer

## Setup

### MongoDB

```
Download MongoDB from https://www.mongodb.com
Go to the MongoDB bin folder
Example: C:\Program Files\MongoDB\Server\3.2\bin
mongod --dbpath C:\path\to\data\folder (this is a random empty folder where you choose to put your data, it must exist)
Wait for [initandlisten] and you're good to go! Keep it running while the application runs.
```

### Usage

```
npm install
npm start
Open up http://localhost:3000 in your browser
```

### Error message on npm start
You'll probably get the following message when running npm start

```
js-bson: Failed to load c++ bson extension, using pure JS version
```
**The application will still run fine despite the error**

It means that the MongoDB module tried to create a couple of files using Python v2.7. 

If you don't happen to have that installed it will fall back on a pure JS version which runs a little slower but 

I figured it would be fine for this small project.

## Design

### Regarding MongoDB

Even though MongoDB has to be installed for this program to run

I figured the benefits would be worth it.
MongoDB provides a lightweight database model and super fast lookups.

### Implementation

The main idea behind the implementation was fairly simple.

Using Express and Node to host the server that can both serve the main website and provide some end points.

Use Node's file system API to scan through the system's file system and locate all images by checking the file MIME type.

The website uses pagination to serve a list of 50 images per page.

There is only one endpoint: /filesjson

It serves the files along with the metadata in JSON format through pagination.

The endpoint accepts two query string parameters: take and page.

Take indicates how many items should be returned, default is 10.

Page indicates which page should be returned, default is 1.

Example: http://localhost:3000/filesjson?page=1&take=3

Example response: 

```
{
	"count":38356,
	"files":
		[{
			"_id":"572f7ef58d6a58582e059b4b",
			"index":"C:\\Users\\Atli\\Pictures\\12745587_10153897176914795_2156819644711746824_n.jpg",
			"stats":
				{
					"dev":1548084132,
					"mode":33206,
					"nlink":1,"uid":0,
					"gid":0,"rdev":0,
					"blksize":null,
					"ino":7036874417772171,
					"size":41345,
					"blocks":null,
					"atime":"2016-02-12T16:35:55.868Z",
					"mtime":"2016-02-12T16:36:01.507Z",
					"ctime":"2016-02-12T16:36:01.507Z",
					"birthtime":"2016-02-12T16:36:01.207Z"
				},
			"mimeType":"image/jpeg"
		}]
}
```