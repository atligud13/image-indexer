var fs = require('fs');
var path = require('path');
var mime = require('mime');

/* Helper function that takes care of walking through all
   directories starting from dir, finding all images and
   saving them to the db */
var walk = function(dir, collection, done) {
  var results = [];
  fs.readdir(dir, function(err, list) {
	if (err) return done(err);
	var pending = list.length;
	if (!pending) return done(null, results);
	list.forEach(function(file) {
	  file = path.resolve(dir, file);
	  fs.stat(file, function(err, stat) {
		if (stat && stat.isDirectory()) {
		  walk(file, collection, function(err, res) {
			results = results.concat(res);
			if (!--pending) done(null, results);
		  });
		}	else {
		  var m = mime.lookup(file);
		  if(m.indexOf('image') > -1) {
			/* Inserting the image to the database */
			collection.insert({
				'index': file,
				'stats': stat,
				'mimeType': m
			}, function (err, doc) {
				if(err) {
					/* Failed inserting to database */
					console.log(err);
				}
			});
		  }
		  if (!--pending) done(null, results);
		}
	  });
	});
  });
};

module.exports = walk;